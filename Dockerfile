# Set the working directory
FROM node:lts
WORKDIR /code

# Copy the package.json and angular.json files into the container
COPY ./package.json ./
COPY ./angular.json ./
COPY ./tsconfig.json ./
COPY ./tsconfig.app.json ./
COPY ./src/assets/ ./src/assets/
COPY ./src/app/ ./src/app/
COPY ./src/index.html ./src
COPY ./src/styles.css ./src
COPY ./src/main.ts    ./src

# Install dependencies
RUN npm install

# Set the environment variable to point to the angular.json file
ENV NG_CLI_ANALYTICS="false"
ENV NG_FILE_PATH="./angular.json"

# Expose the port
EXPOSE 4200

# Start the application
CMD ["npx", "ng", "serve", "--host", "0.0.0.0"]
