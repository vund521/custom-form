import {Injectable, NgModule} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  DetachedRouteHandle,
  PreloadAllModules,
  RouteReuseStrategy,
  RouterModule,
  Routes
} from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'article',
    pathMatch: 'full',
  },
  // {
  //   path: 'user',
  //   loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule)
  // }
];
@Injectable()
class MyStrategy extends RouteReuseStrategy {
  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }
  store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {

  }
  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    return false;
  }
  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    return {};
  }
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return false;
  }

}

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    // preload all modules; optionally we could
    // implement a custom preloading strategy for just some
    // of the modules (PRs welcome 😉)
    preloadingStrategy: PreloadAllModules,
    enableTracing: false,
    onSameUrlNavigation: 'reload'
  })],
  providers: [
    { provide: RouteReuseStrategy, useClass: MyStrategy }
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

