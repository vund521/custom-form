import { Component } from '@angular/core';
import {I18nService} from "translate-localization";
import { OAuthService } from 'angular-oauth2-oidc';
import {authCodeFlowConfig} from "./core/configs/oauth.config";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // constructor(protected fb: FormBuilder, protected cdkDialogService: CdkDialogService) {
  // }

  constructor(
    private i18nService : I18nService,
    // private oauthService: OAuthService
    ) {
    i18nService.useDefaultLang();
  }

  async loginImplicit() {
    // Tweak config for implicit flow
    // this.oauthService.configure(authCodeFlowConfig);
    // const data = await this.oauthService.loadDiscoveryDocument();
    // console.log(data)
    // sessionStorage.setItem('flow', 'implicit');
    //
    // this.oauthService.initLoginFlow('/some-state;p1=1;p2=2?p3=3&p4=4');
    // // the parameter here is optional. It's passed around and can be used after logging in
  }
}

// import {FormBuilder, FormGroup, Validators} from "@angular/forms";
// import {CdkDialogService} from "./share/cdk-dialog/cdk-dialog-service.service";
// import {TestDialogComponent} from "./components/test-dialog.component";

// openDialog(): void{
//   const ref = this.cdkDialogService.open(TestDialogComponent);
//  ref.afterClosed.subscribe(res => {
//    // console.log(res)
//  })
// }
