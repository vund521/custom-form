import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CommonModule} from "@angular/common";
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { ToastrModule } from 'ngx-toastr';

import {ShareModule} from "./share/share.module";
import { AppRoutingModule } from './app-routing.module';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { OAuthModule } from 'angular-oauth2-oidc';

import { AppComponent } from './app.component';
import { TestDialogComponent } from './components/test-dialog.component';

import providerRoot from "./core/configs/provider-root";
import CONFIG_I18N from "./core/configs/i18n-config";
import {I18nModule} from "translate-localization";
import {UsersModule} from "./modules/users/users.module";
import {DemoModule} from "./modules/demo/demo.module";
import {StoreModule} from "@ngrx/store";
import {store, TStore} from "./redux/store";
import {EffectsModule} from "@ngrx/effects";
import {TaskEffectService} from "./redux/effects/task.effect.service";

@NgModule({
  declarations: [
    AppComponent,
    TestDialogComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    ShareModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    LoadingBarModule,
    UsersModule,
    I18nModule.forRoot(CONFIG_I18N),
    ToastrModule.forRoot(),
    DemoModule,
    StoreModule.forRoot(store),
    EffectsModule.forRoot([TaskEffectService]),
    // OAuthModule.forRoot()
  ],
  providers: providerRoot,
  bootstrap: [AppComponent]
})
export class AppModule { }

