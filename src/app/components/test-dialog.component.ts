import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { CdkDialogRef } from 'share/cdk-dialog/cdk-dialog-ref';
import { CdkDialogConfig } from 'share/cdk-dialog/cdk-dialog.config';

@Component({
  selector: 'app-test-dialog-content',
  template: `
    <p>
      test-dialog-content works!
    </p>
    <button class="btn btn-success btn-sm" (click)="onSave()">Save</button>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestDialogComponent implements OnInit {

  constructor(private readonly dialogRef: CdkDialogRef, private readonly dialogConfig: CdkDialogConfig) {
  }

  ngOnInit(): void {
  }

  onSave() {``
    this.dialogRef.close('I saved');
  }
}
