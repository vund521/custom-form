import "reflect-metadata";

const metadataKey = Symbol('title');
const metadataDesKey = Symbol('description');
const setTitle = (value: string) => {
  return Reflect.metadata(metadataKey, value);
}

const getTitle = (Obj: any, propertyKey: string) => {
  return Reflect.getMetadata(metadataKey, Obj, propertyKey)
}

const description = (value: string) => {
  return Reflect.metadata(metadataKey, value)
}
const selectTitle = (Obj: any) => {
  let arr: {label: string, value: any}[] = []
  for (let key in Obj){
    if (key in Obj)
      arr.push({
        label: getTitle(Obj, key),
        value: Obj[key]
      })
  }
  return arr
}

export {
  setTitle,
  getTitle,
  selectTitle,
  description
}
