import {Directive, inject, OnDestroy} from "@angular/core";
import {Subject} from "rxjs";
import {ToastrService} from "ngx-toastr";
import {I18nService} from "translate-localization";

@Directive()
abstract class UnSubscribeBaseComponent implements OnDestroy{
  unSubscribed$ = new Subject<void>();

  private static RequiredCallSubject = new (class RequiredCallSubject {
    private _ = '';
  });

  ngOnDestroy() {
    this.unSubscribed$.next();
    this.unSubscribed$.complete();
    return BaseComponent.RequiredCallSubject;
  }


}

@Directive()
export abstract class BaseComponent extends UnSubscribeBaseComponent{
  private toastr = inject(ToastrService);
  private i18n = inject(I18nService);
  constructor() {
    super();
  }
  showSuccess(params: {message: string, title: string, timeOut: number, isTranslate: boolean}  | string, title: string = '', timeOut: number = 2000){
      if (typeof params !== "string"){
        let {message, title, timeOut} = params
        if (!timeOut)
          timeOut = 2000;

        this.toastr.success(message, title, {
          timeOut
        });
      }else
        this.toastr.success(params, title, {
          timeOut
        });
  }

  showSuccessI18(params: {message: string, title: string, timeOut: number, isTranslate: boolean}  | string, title: string = '', timeOut: number = 2000){
    if (typeof params !== "string"){
      let {message, title, timeOut} = params
      if (!timeOut)
        timeOut = 2000;

      this.toastr.success(this.i18n.translate(message), this.i18n.translate(title), {
        timeOut
      });
    }else
      this.toastr.success(this.i18n.translate(params), this.i18n.translate(title), {
        timeOut
      });
  }

}
