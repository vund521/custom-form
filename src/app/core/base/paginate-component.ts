import {Directive} from "@angular/core";
import {BaseComponent} from "./base-component";

export abstract class PaginateComponent<TData = any> extends BaseComponent{
  protected constructor() {
    super();
  }

  public pageIndex = 1;
  public pageSize = 10;
  public totalRecord = 0;
  public dataSource: TData[] = [];
  setPageIndex(pageIndex: number){
    if(pageIndex < 1 || (pageIndex - 1)*this.pageSize > this.totalRecord)
      return

    this.pageIndex = pageIndex;
    this.loadDataItems();
  }

  setPageSize(pageSize: number){
    this.pageSize = pageSize;
    this.loadDataItems()
  }

  pageNo(index: number){
    return `${(this.pageIndex-1)*this.pageSize + index}` ;
  }

  loadDataItems(){}

}
