import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {IFomControl, IFormGroup} from "core/interfaces";

const fieldsToFormGroup = (fields: IFormGroup[]) : FormGroup => {
  const fb = new FormBuilder();
  let group: any = {};
  fields.forEach(control => {
    const {dataForm, typeForm, name} = control;
    if (name){
      if (typeForm && dataForm){
        if (typeForm === 'form-group')
          group[name] = fieldsToFormGroup(dataForm);
        else if (typeForm === 'form-array')
          group[name] = addFormControlOrFormGroupIntoFormArray(dataForm);
      }
      else {
        group[name] = formControl(control);
      }
    }
  });
  return fb.group(group);
}

const addFormControlOrFormGroupIntoFormArray = (fields: IFormGroup<IFomControl>[]): FormArray => {
  let formArray: any = [], fb = new FormBuilder();
  fields.forEach((control)  => {
    const formGroup = control as IFormGroup;
    const {dataForm, typeForm} = formGroup;

    if (typeForm && dataForm){
      if (typeForm === 'form-group' && control.name){
        const group: any = {};
        group[control.name] = fieldsToFormGroup(dataForm);
        formArray.push(fb.group(group));
      }
      else if (formGroup.typeForm === 'form-array'){
        formArray.push(addFormControlOrFormGroupIntoFormArray(dataForm));
      }
      else {
        formArray.push(formControl(control));
      }
    }
    else {
      formArray.push(formControl(control));
    }
  });

  return fb.array(formArray);
}

const formControl = (control: IFomControl): [any, Validators | undefined] => {
  return [control.value, control.rules]
}

export default fieldsToFormGroup;
