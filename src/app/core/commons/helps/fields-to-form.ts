import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {TFormArray, TFormGroup} from "core/types/form";

export const fieldsToFormGroup = (fields: TFormGroup[]): FormGroup => {
  let group: any = {}, fb = new FormBuilder();
  fields.forEach((field: TFormGroup) => {
    if ("name" in field) {
      if ("group" in field)
        group[field.name] = fieldsToFormGroup(field.group);

      else if ("array" in field)
        group[field.name] = fieldsToFormArray(field.array);

      else group[field.name] = control(field);
    }

  });

  return fb.group(group);
}

export const fieldsToFormArray = (fields: TFormArray[]): FormArray => {
  let array: any[] = [], fb = new FormBuilder();
  fields.forEach(field => {
    if ("name" in field) {
      if ("group" in field)
        array.push(fieldsToFormGroup(field.group));

      else if ("array" in field)
        array.push(fieldsToFormArray(field.array));

      else {
        array.push(control(field));
      }
    } else {
      array.push(control(field));
    }
  });

  return fb.array(array);
}

function control(field: { value: string, rules?: Validators[] } | any) {
  if ("rules" in field)
    return [field.value, field.rules];

  return [field.value];
}


