const getLocal = <T = any>(key: string) =>
  localStorage.getItem(key) as T;

const getObjectOrArray = <T = any>(key: string) =>
  JSON.parse(getLocal<string>(key)) as T;


const setLocal = <T>(key: string, value: T) =>
  localStorage.setItem(key, JSON.stringify(value));

const deleteLocal = (key: string) =>
  localStorage.removeItem(key);

const clearLocal = () => localStorage.clear();

export {
  getLocal,
  getObjectOrArray,
  setLocal,
  deleteLocal,
  clearLocal
}
