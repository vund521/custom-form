import {Icon} from "core/icons";

type T_ICON =  keyof typeof Icon;

 type T_Rule = 'min' | 'max' | 'required' | 'requiredTrue' | 'email' | 'minlength' | 'maxlength' | 'pattern' | 'nullValidate' | 'compose' | 'composeAsync';

 type T_Input = 'checkbox' | 'email'| 'hidden' | 'number' | 'password'| 'text' | 'time' |'search' | 'url' | 'week';

 type T_ResultInput = string | boolean | number | File | null | undefined

 type TData = {[key: string]: any}

export {T_ICON, T_Rule, T_Input, T_ResultInput, TData}
