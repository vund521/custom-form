import {Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {ActionTable, ColumnTable} from "core/interfaces/column-table";
import {NgForOf, NgIf} from "@angular/common";
import {TData} from "core/commons/type";
import {CoreModule} from "core/core.module";

type Render<T= any> = {view: string, action?: (row: T, index: number) => any}
@Component({
  selector: 'common-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  imports: [
    NgIf,
    NgForOf,
    CoreModule
  ],
  standalone: true
})
export class DataTableComponent<T = any> implements OnChanges {
  @Input() dataSource: ({render?: Render[]} & TData)[] = [];
  @Input() columns: ColumnTable<TData | any>[] = [];
  @Input() actions: ActionTable[] = []
  @Input('isChecked') isCheckBox = false

  isChecked = false;
  results: number[] = []

  data: ({render?: Render[]} & TData)[] = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.dataSource.forEach((item, index) => {
      let render: Render[] = [];

      this.columns.forEach(el => {
        let row: Render = {} as Render

        if (el.action)
          row.action = el.action;

        if(el.render)
          row.view = el.render(item, index)

        else if (String(el.dataIndex) in item)
          row.view = item[String(el?.dataIndex)]

        render.push(row)
      })

      this.data.push({
        ...item,
        render
      })
    })
  }



  handleCheckAll(){
    this.isChecked = !this.isChecked;

    // if (this.isChecked){
    //   this.results = this.dataSource.map(item => item['id']);
    // }
    // else {
    //   this.results = [];
    // }
  }

  handleChecked(id: number): boolean{
    const hasId = this.results.includes(id);
    return hasId;
  }

  handleCheckedRow(id: number){
    const hasId = this.handleChecked(id);

    if (hasId){
      this.results = this.results.filter(item => id != item);
      this.isChecked = false;
    }else {
      this.results.push(id);

      if (this.results.length === this.dataSource.length){
        this.isChecked = true;
      }
    }
  }
}
