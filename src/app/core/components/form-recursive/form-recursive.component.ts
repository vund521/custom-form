import { Component, Input} from '@angular/core';
import {IFormGroup} from "core/interfaces/form-group";
import {FormArray, FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-form-recursive',
  templateUrl: './form-recursive.component.html',
  styleUrls: ['./form-recursive.component.css'],
})
export class FormRecursiveComponent {
   @Input({required: true}) fields: IFormGroup[] = [];
   @Input({required: false, alias: 'formGroupRecursive'}) formGroup: FormGroup = new FormGroup<any>('');
   @Input({required: false}) isTranslate : boolean = false;
  getFormGroup (preKeyName: string, nextKeyName?: string, index = 0){
    if (!nextKeyName)
      return this.formGroup.get(preKeyName) as FormGroup;

    return (this.formGroup.get(preKeyName) as FormArray).controls[index].get(nextKeyName) as FormGroup;
  }

  getFormControl(preKeyName: string, index: number) {
    return (this.formGroup.get(preKeyName) as FormArray).controls[index] as FormControl;
  }
}
