import { Component, Input} from '@angular/core';
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {AsyncPipe, JsonPipe, NgForOf, NgIf, NgTemplateOutlet} from "@angular/common";
import {TFormGroup} from "core/types/form";
import {CoreModule} from "core/core.module";
import {getValue, isSet} from "util/function-common";
import {I18nModule} from "translate-localization";

@Component({
  selector: 'form-template',
  templateUrl: './form-template.component.html',
  styleUrls: ['./form-template.component.css'],
  imports: [
    NgTemplateOutlet,
    ReactiveFormsModule,
    I18nModule,
    AsyncPipe,
    NgForOf,
    NgIf,
    JsonPipe,
    CoreModule
  ],
  standalone: true
})
export class FormTemplateComponent<T = TFormGroup> {
   @Input({required: true}) fields: TFormGroup[] = [];
   @Input({required: false, alias: 'formGroupRecursive'}) formGroup: FormGroup = new FormGroup<any>('');
   @Input({required: false}) isTranslate : boolean = false;

  protected readonly getValue = getValue<T>;
  protected readonly isSet = isSet<T>;
  getFormGroup (preKeyName: string, nextKeyName?: string, index = 0){
    if (!nextKeyName)
      return this.formGroup.get(preKeyName) as FormGroup;
    return (this.formGroup.get(preKeyName) as FormArray).controls[index].get(nextKeyName) as FormGroup;
  }

  getFormControl(preKeyName: string, index: number) {
    return (this.formGroup.get(preKeyName) as FormArray).controls[index] as FormControl;
  }

  getFormArray(preKeyName: string, indexArray: number): FormGroup{
    return (this.formGroup.get(preKeyName) as FormArray).controls[indexArray] as FormGroup;
  }
}
