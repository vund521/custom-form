import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputShareComponent } from './input-core.component';

describe('InputShareComponent', () => {
  let component: InputShareComponent;
  let fixture: ComponentFixture<InputShareComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InputShareComponent]
    });
    fixture = TestBed.createComponent(InputShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
