import {Component, forwardRef, Input, ViewChild, ElementRef } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator
} from "@angular/forms";
import {T_Input, T_ResultInput} from "core/commons/type";
import {
  IMessageError
} from "core/interfaces/Form";

@Component({
  selector: 'app-input',
  templateUrl: './input-core.component.html',
  styleUrls: ['./input-core.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputCoreComponent,
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputCoreComponent),
      multi: true
    }
  ]
})

export class InputCoreComponent implements ControlValueAccessor, Validator {
  @ViewChild('dateTimeInput') dateTimeInput!: ElementRef<HTMLInputElement>;
  @Input() type: T_Input | undefined = 'text';
  @Input() messageErrors: IMessageError[] | undefined = [];
  @Input() disabled: boolean = false;
  @Input() isTranslate: boolean = false;
  @Input('min') minRange: number = 0;
  @Input('max') maxRange: number = 100;
  public valueInput: T_ResultInput;
  public control: FormControl = new FormControl();
  public valueModel: string = '';
  onTouched = () => {};
  onChange = (fn: any) => {};

  writeValue(value: T_ResultInput) {
    this.valueInput = value;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
  validate(control: AbstractControl) {
    this.control = control as FormControl;
    return control;
  }

  handleChangeValue(event: Event) {
    const value: string = (event.target as HTMLInputElement).value;
    this.writeValue(value);
    this.onChange(value);
  }

  handleChangeCheckBox(){
    this.writeValue(!this.control.value);
    this.onChange(!this.control.value);
  }
}


