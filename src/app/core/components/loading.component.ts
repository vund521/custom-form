import { ChangeDetectionStrategy, Component } from '@angular/core';
import {CoreModule} from "../core.module";

@Component({
  selector: 'app-loading',
  standalone: true,
  template: `
    <div class="dark-backdrop d-flex align-items-center justify-content-center">
      <span core-icon type="loading-icon"></span>
    </div>
  `,
  styles: [],
  imports: [
    CoreModule
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingComponent {

}
