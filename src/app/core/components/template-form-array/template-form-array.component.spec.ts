import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateFormArrayComponent } from './template-form-array.component';

describe('TemplateFormArrayComponent', () => {
  let component: TemplateFormArrayComponent;
  let fixture: ComponentFixture<TemplateFormArrayComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateFormArrayComponent]
    });
    fixture = TestBed.createComponent(TemplateFormArrayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
