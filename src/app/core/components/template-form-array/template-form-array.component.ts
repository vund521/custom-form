import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {TFormArray, TFormGroup} from "core/types/form";
import {FormArray, FormControl, FormGroup, ReactiveFormsModule} from "@angular/forms";
import {AsyncPipe, JsonPipe, NgForOf, NgIf, NgTemplateOutlet} from "@angular/common";
import {CoreModule} from "core/core.module";
import {FormTemplateComponent} from "../form-template/form-template.component";
import {getValue, isSet} from "util/function-common";
import {I18nModule} from "translate-localization";

@Component({
  selector: 'template-form-array',
  templateUrl: './template-form-array.component.html',
  styleUrls: ['./template-form-array.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgForOf,
    NgIf,
    NgTemplateOutlet,
    CoreModule,
    ReactiveFormsModule,
    I18nModule,
    AsyncPipe,
    FormTemplateComponent,
    JsonPipe
  ],
  standalone: true
})
export class TemplateFormArrayComponent<T = TFormGroup> {
  @Input({required: true}) fields: TFormArray[] = [];
  @Input({required: false, alias: 'formArrayRecursive'}) formArray: FormArray = new FormArray<any>([]);
  @Input({required: false}) isTranslate : boolean = false;

  getControl(index: number): FormControl{
    return this.formArray.controls[index] as FormControl
  }

  getFormGroup(index: number): FormGroup{
    return this.formArray.controls[index] as FormGroup
  }

  getFormArray(index: number): FormArray {
    return this.formArray.controls[index] as FormArray;
  }

  protected readonly isSet = isSet;
  protected readonly getValue = getValue;
}
