import {IEnviroment} from "core/interfaces/enviroment";

export const environment: IEnviroment = {
  production: true,
  apiUrl: 'stagingKey'
};
