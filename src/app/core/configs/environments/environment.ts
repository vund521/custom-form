import {IEnviroment} from "core/interfaces/enviroment";

export const environment: IEnviroment = {
  production: false,
  apiUrl: 'https://6576bbc7424e2adad5b48cea.mockapi.io/'
};
