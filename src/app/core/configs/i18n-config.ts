import {II18nConfig} from "translate-localization";

const CONFIG_I18N : II18nConfig = {
  folders: ['root', 'users', 'buttons'], // load file langauges in i18n
  defaultLanguage: 'vi'
}

export default CONFIG_I18N
