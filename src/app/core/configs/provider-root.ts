import {Provider} from "@angular/core";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {GLOBAL_STATE} from "core/commons/constants";

import {StoreBase} from "core/store/store-base";

import {UserService} from "services/user.service";

const providerRoot: Provider[]  = [
  {
    provide: GLOBAL_STATE,
    useFactory: (us: UserService) => new StoreBase(us),
    deps: [UserService]
  },
  BrowserAnimationsModule, // required animations module
  // { provide: LOADING_BAR_CONFIG, useValue: { latencyThreshold: 10000 } },
];

export default providerRoot;
