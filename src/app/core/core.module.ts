import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from "@angular/forms";
import { CommonModule } from '@angular/common';
import {IconDirective} from "./directives/icon.directive";
import {InputCoreComponent, FormRecursiveComponent} from "./components";
import {I18nModule} from "translate-localization";
import { DataTableComponent } from './components/data-table/data-table.component';

@NgModule({
  declarations: [
    IconDirective,
    InputCoreComponent,
    FormRecursiveComponent,
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        I18nModule,
    ],
  exports: [
    IconDirective,
    InputCoreComponent,
    FormRecursiveComponent,
  ]
})
export class CoreModule { }
