import {IFmpeg} from "core/interfaces/ffmpeg";

declare const FFmpegWASM:  { FFmpeg: new () => IFmpeg }
export default FFmpegWASM.FFmpeg;
