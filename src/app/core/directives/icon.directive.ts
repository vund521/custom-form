import {Directive, ElementRef, Input, OnChanges} from '@angular/core';
import {Icon} from "core/icons";
import {T_ICON} from "../commons/type";
@Directive({
  // standalone: true,
  selector: '[core-icon]'
})
export class IconDirective implements OnChanges {
  @Input('type') keyObj: T_ICON | undefined;
  @Input() size: number | string | unknown;
  constructor(private el: ElementRef) {}

  ngOnChanges(){
    if (this.keyObj)
      this.el.nativeElement.innerHTML = Icon[this.keyObj];

    if (this.size != undefined){
      this.el.nativeElement.children[0].attributes[0].value = this.size;
      this.el.nativeElement.children[0].attributes[1].value = this.size;
    }
  }

}
