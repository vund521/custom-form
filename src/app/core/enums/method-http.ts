export enum MethodHttp {
  Get = 'Get',
  Post = 'Post',
  Put = 'Put',
  Delete = 'Delete',
  GetFileBold = 'GetFileBold'
}
