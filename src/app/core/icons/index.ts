import ExamIcon from "./exam-icon";
import MOVE_ICON from "./move-icon";

export const Icon = {
  'exam-icon' : ExamIcon,
  'move-icon' : MOVE_ICON
}
