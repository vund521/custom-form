import {T_ICON, TData} from "core/commons/type";

export interface ColumnTable<T = TData | any> {
  title: string,
  dataIndex?: keyof T,
  render?: (row: T, index: number) => string,
  action?: (row: T, index: number) => void
}

export interface ActionTable<TData = any> {
  icon: T_ICON,
  action: (item: TData, index: number) => void,
  label: string,
  className?: string
}
