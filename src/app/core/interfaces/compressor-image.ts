declare interface ICompressorImage {
  compress (t: number, e: number, n?: string): HTMLImageElement;
}

export default ICompressorImage
