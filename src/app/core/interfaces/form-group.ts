import {Validators} from "@angular/forms";
import {T_Input, T_Rule} from "../commons/type";
import {Observable} from "rxjs";

// export interface IFormGroup<T, U = any> extends IFormGroupGeneric {
//   dataForm?: (T | U)[]
// }
//
//
export interface IFormGroup<T = any> extends IFormGroupGeneric {
  dataForm?: IFormGroup<T>[]
}

export interface IFormGroupGeneric extends IFomControl{
  typeForm?: 'form-group' | 'form-array' | 'form-control',
  label?: string | Observable<any>,
  name?: string,
  inputType?: T_Input,
}

export interface IFomControl extends IValidator{
  value?: any,
}

interface IValidator{
  rules?: Validators,
  messageErrors?: IMessageError[]
}

export interface IMessageError {
  type: T_Rule,
  message: string | Observable<any>,
}

