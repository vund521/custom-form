import {Observable} from "rxjs";
import {Validators} from "@angular/forms";
import {T_Input, T_Rule} from "core/commons/type";
import {TFormArray, TFormGroup} from "core/types/form";

interface LabelName{
  label: string | Observable<any>,
  name: string,
}
interface FormGroup extends LabelName{
  group: TFormGroup[]
}

interface FormArray  extends LabelName {
  array: TFormArray[]
}

//form control
interface FormControl {value: any}

interface FormControlNotValidate extends FormControl, LabelName{inputType?: T_Input}

interface FormControlHasValidate extends FormControlNotValidate, FormControl, IValidator {}

interface FormControlNotNameHasValidate extends  IValidator, FormControl{}

interface IValidator{
  rules: Validators,
  messageErrors: IMessageError[]
}
interface IMessageError {
  type: T_Rule,
  message: string | Observable<any> | any,
}

export {
  LabelName,
  FormGroup,
  FormArray,
  FormControl,
  FormControlNotValidate,
  FormControlHasValidate,
  FormControlNotNameHasValidate,
  IValidator,
  IMessageError
}

