import {OptionHttp} from "./option-http";

type OptionHttpModel<T = any> = HttpModel | OptionModel | BodyModel<T> | OptionBodyModel<T> | CacheModel;

interface HttpModel {
  url: string;
}

interface CacheModel extends HttpModel{
  isCache : boolean
}

interface OptionModel extends HttpModel{
  options: OptionHttp
}

interface OptionBodyModel<T = any> extends HttpModel, BodyModel<T>{}

interface BodyModel<T = any> extends HttpModel{
  body: T
}

export {
  OptionHttpModel,
  HttpModel,
  OptionModel,
  OptionBodyModel,
  BodyModel
}
