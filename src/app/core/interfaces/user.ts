export interface IUser {
  "createdAt": string,
  "full_name": string,
  "age": number,
  "email": string,
  "address": string,
  "user_name": string,
  "password": string,
  "id": string | number
}
