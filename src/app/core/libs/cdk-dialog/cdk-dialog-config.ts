import {OverlayConfig} from "@angular/cdk/overlay";

export class CdkDialogConfig<TData = any> {
  header= '';
  height?: string | number = '30vh';
  width?: string | number = '60wh'
  data?: TData;
  closeable? = true; // xac dinh dialog dong hoac khong
  containerAnimationTiming? = 0.3; // fade
  contentAnimationTiming? = 0.2; // zoom
  animationChildDelay? = 0;
  overlayConfig?: OverlayConfig;
  isTranslate?: boolean = false;
  turnOffHeader?: boolean = false;
}
