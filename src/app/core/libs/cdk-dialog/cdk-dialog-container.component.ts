import {
    ChangeDetectorRef,
    Component,
    ComponentRef,
    Inject, OnDestroy,
    ViewChild,
    ViewContainerRef
} from '@angular/core';
import {CdkDialogConfig} from "./cdk-dialog-config";
import {CDK_DIALOG_CONFIG} from "./token-inject";
import {NgIf} from "@angular/common";
import {CdkDialogRef} from "./cdk-dialog-ref";
import {Observable} from "rxjs";

@Component({
  selector: 'app-cdk-dialog-container',
  standalone: true,
  template: `
    <div class="card" >
      <div *ngIf="!cdkDialogConfig.turnOffHeader" class="card-header d-flex justify-content-between align-items-center">
        {{cdkDialogConfig.header || ''}}
        <button *ngIf="!cdkDialogConfig.closeable" type="button" class="close" aria-label="Close" (click)="cdkDialogRef.close()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="card-body">
        <ng-container #cdk_container></ng-container>
      </div>
    </div>
  `,
  imports: [NgIf]
})
export class CdkDialogContainerComponent<TContentComponent = any> implements OnDestroy{
  @ViewChild('cdk_container', { read: ViewContainerRef })
  private viewContainerRef!: ViewContainerRef;

  typeContentComponent: TContentComponent | any;
  componentRef!: ComponentRef<any>
  constructor(@Inject(CDK_DIALOG_CONFIG)
              protected readonly cdkDialogConfig: CdkDialogConfig,
              private readonly cdr: ChangeDetectorRef,
              protected readonly cdkDialogRef: CdkDialogRef) {}

  ngAfterViewInit() {
    this.viewContainerRef.clear();
    this.componentRef = this.viewContainerRef.createComponent(this.typeContentComponent);
    this.componentRef.instance.data = this.cdkDialogConfig?.data;
    this.componentRef.changeDetectorRef.detectChanges();
    this.cdr.markForCheck();
  }

  get afterClose(): Observable<any>{
      return this.cdkDialogRef.afterClose;
  }

  ngOnDestroy(): void {
      this.componentRef?.destroy();
  }

}
