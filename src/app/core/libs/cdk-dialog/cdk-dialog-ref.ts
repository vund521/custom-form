import {map, Observable, Observer, race, Subject} from "rxjs";
import {OverlayRef} from "@angular/cdk/overlay";
import {filter} from "rxjs/operators";
import {ESCAPE, hasModifierKey} from "@angular/cdk/keycodes";

export class CdkDialogRef {
  private afterClose$ = new Subject();
  constructor(private readonly overlayRef: OverlayRef) {
    race(
      overlayRef.backdropClick().pipe(map(() => undefined)),
      overlayRef.keydownEvents().pipe(filter(event =>
              event.keyCode === ESCAPE && !hasModifierKey(event)),
          map(() => undefined))
    ).subscribe(res => this.close(res));
  }

  get afterClose(): Observable<any>{
    return this.afterClose$.asObservable();
  }

  close(data?: any){
    this.overlayRef.dispose();
    this.overlayRef.detach();
    this.afterClose$.next(data);
    this.afterClose$.complete();
  }
}
