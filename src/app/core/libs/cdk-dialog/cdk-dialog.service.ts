import {Injectable, Injector, Type} from '@angular/core';
import {Overlay, OverlayConfig, OverlayRef} from "@angular/cdk/overlay";

import {CdkDialogConfig} from "./cdk-dialog-config";
import {ComponentPortal} from "@angular/cdk/portal";
import {CdkDialogContainerComponent} from "./cdk-dialog-container.component";
import {CDK_DIALOG_CONFIG, DIALOG_DATA} from "./token-inject";
import {CdkDialogRef} from "./cdk-dialog-ref";
``
@Injectable({
  providedIn: 'root'
})
export class CdkDialogService {
  private readonly overlayConfig = new OverlayConfig({
    positionStrategy: this.overlay.position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    hasBackdrop: true,
    backdropClass: 'dark-backdrop',
    panelClass: 'overlay-pane-class',
    scrollStrategy: this.overlay.scrollStrategies.block(),
  });
  constructor(private overlay: Overlay) { }

  open(component: Type<any>, cdkDialogConfig?: CdkDialogConfig){
    const overlayRef = this.overlay.create(this.overlayConfig),
    inject = this.createInject(overlayRef, cdkDialogConfig);
    const componentRef = new ComponentPortal(CdkDialogContainerComponent, null, inject);
    const portal = overlayRef.attach(componentRef);
    portal.instance.typeContentComponent = component;
    return portal.instance;
  }

  createInject(overlayRef: OverlayRef, cdkDialogConfig?: CdkDialogConfig){
    return Injector.create({
      providers: [
        {
          provide: CDK_DIALOG_CONFIG,
          useValue: cdkDialogConfig || null
        },
        {
          provide: DIALOG_DATA,
          useValue: cdkDialogConfig?.data
        },
        {
          provide: CdkDialogRef,
          useFactory: () => new CdkDialogRef(overlayRef)
        }
      ]
    });
  }
}
