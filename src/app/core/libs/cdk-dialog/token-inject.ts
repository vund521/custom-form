import {InjectionToken} from "@angular/core";

export const CDK_DIALOG_CONFIG = new InjectionToken('overlay-ref');
export const DIALOG_DATA = new InjectionToken('dialog_data');
