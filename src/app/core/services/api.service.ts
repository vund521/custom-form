import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, ReplaySubject, tap} from "rxjs";

import * as moment from "moment";

import {MethodHttp} from "core/enums/method-http";

import {OptionHttpModel} from "core/interfaces/http";

import {CacheService} from "./cache.service";

import {environment} from "core/configs/environments/environment";
import {OptionHttp} from "../interfaces/option-http";

const { apiUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _http = inject(HttpClient);
  private _cacheService = inject(CacheService);

  constructor() { }

  request<T>(params: OptionHttpModel, method: string, isCache: boolean){

    if (isCache)
      return this.cacheApi(params, method);

    return this.fetchApi(params, method);
  }

  private cacheApi<T = any>(params: OptionHttpModel, method: string): Observable<T>{
    const url = params.url;
    if (!this._cacheService.hasKeyReplay(url)){
      return this.fetchApi<T>(params, method).pipe(tap(result => {
        const replay = new ReplaySubject<T>();
        replay.next(result);
        replay.complete();

        const timeLife = moment().add(20, "seconds").format('X');
        this._cacheService.setTimeLifeCache(url, Number(timeLife));
        this._cacheService.setStoreCacheReplay<T>(url, replay)
      }));
    }

    const timeLife = this._cacheService.getTimeLife(url);
    if (this._cacheService.getTimeLife(url) && timeLife <= Number(moment().format('X'))){
      this._cacheService.deleteTimeLife(url);
      this._cacheService.deleteStoreCacheReplace(url);
      return this.cacheApi(params, method);
    }

    return this._cacheService.getStoreReplay<T>(url);
  }

  private fetchApi<T = any>(params: OptionHttpModel, method: string){
    const url = apiUrl + params.url;
    let options = undefined, body = null, resultApi: Observable<T>;
    if ('options' in params){
      options = params.options;
    }

    if ('body' in params)
      body = params.body;


    switch (method) {
      case MethodHttp.Get:
        resultApi = this._http.get<T>(url, options);
        break;

      case MethodHttp.Post:
        resultApi = this._http.post<T>(url, body, options);
        break

      default :
        resultApi = this._http.delete<T>(url, options)
    }

    return resultApi;
  }

  get<T = any>(params: OptionHttpModel, isCache = false){
    return this.request<T>(params, MethodHttp.Get, isCache);
  }

  put<T = any>(params: OptionHttpModel, isCache = false){
    return this.request<T>(params, MethodHttp.Delete, isCache);
  }

  delete<T = any>(params: OptionHttpModel, isCache = false){
    return this.request<T>(params, MethodHttp.Delete, isCache);
  }

  post<T = any>(params: OptionHttpModel, isCache = false){
    return this.request<T>(params, MethodHttp.Post, isCache);
  }


}



