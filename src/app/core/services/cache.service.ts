import { Injectable } from '@angular/core';
import { ReplaySubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  private timeLifeCache: Map<string, number>   = new Map();
  private _storeCacheReplay: Map<string, ReplaySubject<any>> = new Map();
  constructor() { }

  setTimeLifeCache<T>(key: string, value: number){
    return this.timeLifeCache.set(key, value);
  }

  getTimeLife(key: string): number{
    return this.timeLifeCache.get(key) as number;
  }

  hasKeyReplay(key: string){
    return this._storeCacheReplay.has(key);
  }

  deleteTimeLife(key: string){
    this.timeLifeCache.delete(key);
  }

  setStoreCacheReplay<T>(key: string, value: ReplaySubject<T>){
    this._storeCacheReplay.set(key, value);
  }

  getStoreReplay<T>(key: string){
    return this._storeCacheReplay.get(key) as ReplaySubject<T>;
  }

  deleteStoreCacheReplace(key: string){
    this._storeCacheReplay.delete(key);
  }

  clearStoreCacheReplace(){
    this._storeCacheReplay.clear();
  }
}


