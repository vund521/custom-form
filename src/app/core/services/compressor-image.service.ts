import { Injectable } from '@angular/core';
import {Observable, Subscriber} from "rxjs";
import jic from "core/declares/jic-compressor";

@Injectable()
export class CompressorImageService {

  constructor() { }

  readFile(file: File, subscriber: Subscriber<any>) {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      subscriber.next(fileReader.result);
      subscriber.complete();
    };

    fileReader.onerror = (error) => {
      subscriber.error(error);
      subscriber.complete();
    };
  }

  private readImageResizeAndCompressPhoto<T>(file: File, subscriber: Subscriber<T>): void { // image javascript element
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    if (file.size <= 6000) {
      fileReader.onload = () => {
        subscriber.next(fileReader.result as T);
        subscriber.complete();
      };
    }

    else {
      fileReader.onload = () => {
        let compress = 90, dataUrl = fileReader.result;
        const resize = () => {
          if (compress <= 50) {
            const img = new Image();
            if (dataUrl && typeof dataUrl === "string"){
              img.src = dataUrl;
              img.onload = () => {
                const canvas: HTMLCanvasElement = document.createElement("canvas");
                // const MAX_WIDTH = 1152;
                // const MAX_HEIGHT = 1152;
                let width = img.width, height = img.height;
                // if (width > height) {
                //   if (width > MAX_WIDTH) {
                //     height *= MAX_WIDTH / width;
                //     width = MAX_WIDTH;
                //   }
                // } else {
                //   if (height > MAX_HEIGHT) {
                //     width *= MAX_HEIGHT / height;
                //     height = MAX_HEIGHT;
                //   }
                // }
                canvas.width = width;
                canvas.height = height;
                const ctx = canvas.getContext("2d");
                if (ctx){
                  ctx.drawImage(img, 0, 0, width, height);
                  const extension = img.src.substring(img.src.indexOf(':') + 1, img.src.indexOf(';'));
                  dataUrl = canvas.toDataURL(extension);
                  subscriber.next(dataUrl as T);
                  subscriber.complete();
                }
              };
            }
            // end resize
          }
          else{
            const photo: any = new Image();
            photo.src = dataUrl;
            photo.onload = () => {
              let extension = photo.src.substring(photo.src.indexOf(':') + 1, photo.src.indexOf(';'));
              photo.src = jic.compress(photo, compress, extension).src;
              photo.onload = () => {
                dataUrl = photo.src;
                compress -= 10;
                return resize();
              };
            }
          }
        };

        return resize();
      };

      fileReader.onerror = (error) => {
        subscriber.error(error);
        subscriber.complete();
      };
    }
  }


  onChangeFile<T = string | ArrayBuffer>(file: File): Observable<T>{
    return new Observable((subscriber: Subscriber<T>) => {
      this.readImageResizeAndCompressPhoto<T>(file, subscriber);
    });
  }
}
