import {ComponentStore} from "core/libs/store/component-store";
import {IUser} from "core/interfaces";
import {UserService} from "services/user.service";
import {tap} from "rxjs";
import {Injectable} from "@angular/core";


const state = {
  currentUser: {} as IUser,
  isLoading: false as boolean
}

type State = typeof state;
export class StoreBase extends ComponentStore<State>{
  constructor(private us: UserService) {
    super(state)
  }

  get currentUser() {
    return this.get().currentUser;
  }

  set currentUser (currentUser){
    this.patchState({currentUser});
  }

  getInFoUser(){
    return this.us.getInfoUser(1).pipe(tap({
      next: (value: any) => {
        this.currentUser = value;
      }
    })).subscribe();
  }

  get isLoading(){
    return this.get().isLoading;
  }

  set isLoading(value: boolean){
    this.patchState({isLoading: value})
  }


}
