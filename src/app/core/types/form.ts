import {
  FormArray, FormControl,
  FormControlHasValidate,
  FormControlNotNameHasValidate,
  FormControlNotValidate,
  FormGroup
} from "core/interfaces/Form";

type TFormGroup = FormGroup | FormArray | TFormControl;
type TFormControl = FormControlNotValidate | FormControlHasValidate | FormControlNotNameHasValidate;
type TFormArray = TFormGroup | FormControl;

export {
  TFormGroup,
  TFormControl,
  TFormArray,
}
