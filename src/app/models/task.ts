export interface Task {
  date: number,
  note: string,
  status: boolean,
  id: number | string
}
