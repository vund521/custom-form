import {Component, inject, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Action, ActionReducer, Store} from "@ngrx/store";
import {store, TStore} from "redux/store";
import {Task} from "models/task";
import * as TaskAction from "redux/actions/task.action";
// import {selectTask} from "redux/selectors/task.selector";
import {TaskService} from "services/task.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-demo-ngrx',
  templateUrl: './demo-ngrx.component.html',
  styleUrls: ['./demo-ngrx.component.css']
})
export class DemoNgrxComponent implements OnInit{

  count$: Observable<ActionReducer<Task[]>>;

  private task = inject(TaskService);
  private router = inject(Router);
  constructor(private store: Store<TStore>) {
    this.count$ = store.select('task');
    // store.select(selectTask()).subscribe(res => {
    //   console.log(res)
    // })
  }

  handleLoadTask() {
    this.store.dispatch(TaskAction.getTask())
  }

  handleAdd(){
    this.store.dispatch(TaskAction.addTask({
      date: 1730345320,
      note: "note 2",
      status: false,
      id: 2
    }))
  }

  redirectRouter(){
    return this.router.navigate(['/user-table'])
  }

  ngOnInit(): void {
  }
}
