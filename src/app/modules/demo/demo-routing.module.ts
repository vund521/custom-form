import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DemoNgrxComponent} from "./demo-ngrx/demo-ngrx.component";

const routes: Routes = [
  {
    path: 'ngrx',
    component: DemoNgrxComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DemoRoutingModule { }
