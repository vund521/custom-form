import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DemoRoutingModule } from './demo-routing.module';
import { DemoNgrxComponent } from './demo-ngrx/demo-ngrx.component';


@NgModule({
  declarations: [
    DemoNgrxComponent
  ],
  imports: [
    CommonModule,
    DemoRoutingModule
  ]
})
export class DemoModule { }
