import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BehaviorSubject} from "rxjs";
import {CompressorImageService} from "core/services/compressor-image.service";
import { NgOptimizedImage } from '@angular/common'

@Component({
  selector: 'app-compressor-image',
  standalone: true,
  imports: [
    CommonModule,
    NgOptimizedImage
  ],
  templateUrl: './compressor-image.component.html',
  styleUrls: ['./compressor-image.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [CompressorImageService]
})
export class CompressorImageComponent {
  url$: BehaviorSubject<string> = new BehaviorSubject('https://i.pinimg.com/originals/2c/84/5a/2c845a66b8ad2a8aafd288bdc16cd459.jpg');
  constructor(private readonly compressorImageService: CompressorImageService) {}
  ngOnInit(){}

  chooseFile(element: string) {
    (document.getElementById(element) as HTMLElement).click();
  }

  onChangeFile(event: Event) {
    const files = (event.target as HTMLInputElement).files;
    if (files && files.length > 0){
      this.compressorImageService.onChangeFile<string>(files[0]).subscribe(url =>
        this.url$.next(url))
    }
  }
}
