import { Component } from '@angular/core';
import {selectTitle} from "core/attributes/@title";
import {enumDecorator} from "./enum-decorator";

@Component({
  selector: 'app-decorator',
  templateUrl: './decorator.component.html',
  styleUrls: ['./decorator.component.css']
})
export class DecoratorComponent {
  constructor() {
    console.log(selectTitle(enumDecorator))
  }
}
