import {description, setTitle} from "core/attributes/@title";

class EnumDecorator {
  @setTitle('Tên người dùng')
  @description('Mô tả người dùng')
  userTitle = '1'

  @setTitle('Tỉnh/ tp')
  @description('Mô tả tên thành phố')
  city = 1;

  @setTitle('Địa chỉ')
  @description('Mô tả địa chỉ')
  address = 3

}

export const enumDecorator = new EnumDecorator()
