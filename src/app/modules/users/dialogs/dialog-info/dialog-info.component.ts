import {Component, Inject, Input} from '@angular/core';
import {Observer} from "rxjs";
import {DIALOG_DATA} from "core/libs/cdk-dialog/token-inject";
import {IUser} from "core/interfaces";
import {CdkDialogRef} from "core/libs/cdk-dialog/cdk-dialog-ref";

@Component({
  selector: 'app-dialog-info',
  templateUrl: './dialog-info.component.html',
  styleUrls: ['./dialog-info.component.css']
})
export class DialogInfoComponent {
  @Input() data!: Partial<IUser>;
  constructor(@Inject(DIALOG_DATA) protected readonly dataRef: Partial<Observer<IUser>>,
              private readonly cdkDialogRef: CdkDialogRef) {
  }

  submitForm(){
    this.cdkDialogRef.close({name: 123, password: 345})
  }
}
