import { ChangeDetectionStrategy, Component } from '@angular/core';
import {CdkDragDrop, DragDropModule, moveItemInArray} from "@angular/cdk/drag-drop";
import {JsonPipe, NgForOf} from "@angular/common";

@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  standalone: true,
  styleUrls: ['./drag-drop.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    DragDropModule,
    NgForOf,
    JsonPipe
  ]
})
export class DragDropComponent {
  items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
  drop(event: CdkDragDrop<any>) {
    this.items[event.previousContainer.data.index] = event.container.data.item;
    this.items[event.container.data.index] = event.previousContainer.data.item;
  }
}
