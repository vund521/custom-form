import {Component, ElementRef, ViewChild} from '@angular/core';
import Drawflow from 'drawflow'
import {dataToImport} from "./init-data";

@Component({
  selector: 'app-draw-flow',
  templateUrl: './draw-flow.component.html',
  styleUrls: ['./draw-flow.component.css']
})
export class DrawFlowComponent {
  @ViewChild('drawFlow',  {read: ElementRef}) drawFlowRef!: ElementRef;
  editor!: Drawflow;
  ngAfterViewInit(){
    this.editor = new Drawflow(this.drawFlowRef.nativeElement);

    this.editor.reroute = true;
    this.editor.reroute_fix_curvature = true;
    this.editor.force_first_input = false;
    this.editor.start();
    this.editor.import(dataToImport)
  }
}
