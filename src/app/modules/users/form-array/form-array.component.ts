import {ChangeDetectionStrategy, Component, inject} from '@angular/core';
import formData from "./init";
import {fieldsToFormArray} from "core/commons/helps/fields-to-form";
import {UserService} from "services/user.service";

@Component({
  selector: 'app-form-array',
  templateUrl: './form-array.component.html',
  styleUrls: ['./form-array.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormArrayComponent {
  private _userService = inject(UserService);
  fields = formData;
  formArray = fieldsToFormArray(formData);
  constructor() {
    this.loadDataItems();
  }

  loadDataItems(){
    this._userService.list().subscribe(res => {
      console.log(res)
    })
  }
}
