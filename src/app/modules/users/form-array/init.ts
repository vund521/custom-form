import {TFormArray} from "core/types/form";
import {Validators} from "@angular/forms";
const {required, minLength, maxLength, email} = Validators;

const formData: TFormArray[] = [
  {
    value: 12,
    rules: [required],
    messageErrors: [
      {
        type: "required",
        message: 'Không được để trông'
      }
    ]
  },
  {
    value: 12,
    rules: [required],
    messageErrors: [
      {
        type: "required",
        message: 'Không được để trông'
      }
    ]
  },
  {
    name: 'group_job',
    label: 'Nhóm công việc',
    group: [
      {
        name: 'name_job',
        label: 'Tên công việc',
        value: 'Angular',
        rules: [required],
        messageErrors: [
          {
            type: "required",
            message: 'Không được để trông'
          }
        ]
      },
      {
        name: 'skill',
        label: 'Kỹ năng',
        value: 'Opp',
        rules: [required],
        messageErrors: [
          {
            type: "required",
            message: 'Không được để trông'
          }
        ]
      }
    ]
  },
  {
    name: 'list_job',
    label: 'Dánh sách công việc',
    array: [
      {
        label: 'Tên công việc',
        value: 'Angular',
        rules: [required],
        messageErrors: [
          {
            type: "required",
            message: 'Không được để trông'
          }
        ]
      },
      {
        label: 'Kỹ năng',
        value: 'Opp',
        rules: [required],
        messageErrors: [
          {
            type: "required",
            message: 'Không được để trông'
          }
        ]
      }
    ]
  }
] satisfies TFormArray[]

export default formData;
