import {ChangeDetectionStrategy, Component, inject, Inject} from '@angular/core';
import {fieldsToFormGroup} from "core/commons/helps/fields-to-form";
import formInit from "./init-form";
import {GLOBAL_STATE} from "core/commons/constants";
import {StoreBase} from "core/store/store-base";
import {TFormGroup} from "core/types/form";
import {I18nService} from "translate-localization";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-form-group',
  templateUrl: './form-group.component.html',
  styleUrls: ['./form-group.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormGroupComponent {
  fields: TFormGroup[] = formInit;
  formData = fieldsToFormGroup(formInit);
  dataStore = this.storeBase.data;
  private _userService = inject(UserService);
  constructor(@Inject(GLOBAL_STATE) private storeBase: StoreBase,
              private i18: I18nService) {
    this.loadDataItem();
  }


  loadDataItem(){
    this._userService.list().subscribe(res => {
      console.log(res);
    });
  }
}
