import {Validators} from "@angular/forms";
import {TFormGroup} from "core/types/form";

const {required, minLength, maxLength, email} = Validators;

const formInit: TFormGroup[] = [
  {
    label: 'users.form_user.name',
    name: 'name',
    value: '123',
    rules: [required, minLength(4), maxLength(8)],
    messageErrors: [
      {type: "required", message: 'users.required'},
      {type: "minlength", message: 'users.minlength_4'},
      {type: "maxlength", message: 'users.maxlength_8'},
    ]
  },
  {
    label: 'users.form_user.email',
    name: 'email',
    value: '456',
    inputType: "email",
    rules: [required, email, minLength(8), maxLength(50)],
    messageErrors: [
      {type: "required", message: 'users.required'},
      {type: "minlength", message: 'users.minlength_4'},
      {type: "maxlength", message: 'users.maxlength_8'},
      {type: "maxlength", message: 'users.email'},
    ]
  },
  {
    label: 'users.form_user.exp_Job',
    name: 'list_job',
    array: [
      {
        label: 'users.form_user.name_job',
        value: '123',
        name: 'a'
      },
      {
        label: 'users.form_user.name_job',
        value: '123',
        name: 'a'
      },
      {
        label: 'Level',
        name: 'level',
        group: [
          {
            label: 'users.form_user.introduce',
            name: 'introduce',
            value: '1234',
          },
          {
            label: 'users.form_user.level',
            value: '123',
            name: 'level'
          }
        ]
      },
    ]
  },
  {
    label: 'users.form_user.info',
    name: 'info_CV',
    group: [
      {
        label: 'users.form_user.introduce',
        name: 'introduce',
        value: '1234'
      },
      {
        label: 'Level',
        name: 'level',
        group: [
          {
            label: 'users.form_user.introduce',
            name: 'introduce',
            value: '1234'
          },
          {
            label: 'Level',
            value: '123',
            name: 'level'
          }
        ]
      },
      {
        label: 'users.form_user.list_tech',
        name: 'list_tech',
        array: [
          {value: 'angular', rules: [required], messageErrors: [{type: "required", message: 'users.required'}]},
          {value: 'angular 2', rules: [required], messageErrors: [{type: "required", message: 'users.required'}]},
        ]
      }
    ]
  }
] satisfies TFormGroup[];

export default formInit;
