import {Component, Inject, OnInit} from '@angular/core';
import {FormGroup, Validators} from "@angular/forms";
import {IFomControl, IFormGroup} from "core/interfaces";
import {StoreBase} from "core/store/store-base";
import {GLOBAL_STATE} from "core/commons/constants";
import fieldsToFormGroup from "core/commons/helps/fields-to-form-group";
import {I18nService, Lang} from "translate-localization";

const {required, minLength, maxLength, email} = Validators;

@Component({
  selector: 'app-list',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.css'],
})
export class FormUserComponent implements  OnInit{
  fields: IFormGroup<IFomControl>[] = [
    {
      label: 'users.form_user.name',
      name: 'name',
      value: '',
      rules: [required, minLength(4), maxLength(8)],
      messageErrors: [
        {type: "required", message: 'Không được để trống'},
        {type: "minlength", message: 'Không ít hơn 4 ký tự'},
        {type: "maxlength", message: 'Không nhiều hơn 8 ký tự'},
      ]
    },
    {
      label: 'users.form_user.email',
      name: 'email',
      value: 'vund521@wru.vn',
      rules: [required, email, minLength(8), maxLength(50)],
      messageErrors: [
        {type: "required", message: 'Không được để trống'},
        {type: "email", message: 'Email không đúng định dạng'},
        {type: "minlength", message: 'Không ít hơn 8 ký tự'},
        {type: "maxlength", message: 'Không nhiều hơn 50 ký tự'},
      ]
    },
    {
      label: 'users.form_user.exp_Job',
      name: 'job_exps',
      typeForm: 'form-array',
      dataForm: [
        {
          label: 'users.form_user.name_job',
          name: 'name_job',
          typeForm: 'form-group',
          dataForm: [
            {
              label: 'users.form_user.email',
              name: 'name_commany',
              value: '1231',
              rules: [required],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
              ]
            },
            {
              label: 'users.form_user.email',
              name: 'position',
              value: '',
              rules: [required, email, minLength(8), maxLength(50)],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
                {type: "minlength", message: 'Không ít hơn 8 ký tự'},
                {type: "maxlength", message: 'Không nhiều hơn 50 ký tự'},
              ]
            },
            {
              label: 'users.form_user.email',
              name: 'income',
              value: '',
              rules: [required],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
              ]
            },
          ]
        },
        {
          label: 'users.form_user.email',
          name: 'other_job',
          typeForm: 'form-group',
          dataForm: [
            {
              label: 'users.form_user.email',
              name: 'name_company',
              value: 'abc',
              rules: [required],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
              ]
            },
            {
              label: 'users.form_user.email',
              name: 'position',
              value: '',
              rules: [required, email, minLength(8), maxLength(50)],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
                {type: "minlength", message: 'Không ít hơn 8 ký tự'},
                {type: "maxlength", message: 'Không nhiều hơn 50 ký tự'},
              ]
            },
            {
              label: 'users.form_user.income',
              name: 'income',
              value: '',
              rules: [required],
              messageErrors: [
                {type: "required", message: 'Không được để trống'},
              ]
            },

          ]
        },
      ],
    },
    {
      label: 'users.form_user.list_address',
      name: 'list-address',
      typeForm: 'form-array',
      dataForm: [
        {
          value: 'Giâm lâm',
          rules: [required],
          messageErrors: [
            {type: "required", message: 'Không được để trống'},
          ]
        },
        {
          value: '',
          rules: [required],
          messageErrors: [
            {type: "required", message: 'Không được để trống'},
          ]
        }
      ]
    },
  ];
  formGroup!: FormGroup;
  dataStore = this.storeBase.data;
  username= '';

  constructor(@Inject(GLOBAL_STATE) private storeBase: StoreBase,
              private i18: I18nService) {
  }
  ngOnInit(){
    this.formGroup = fieldsToFormGroup(this.fields);
    // console.log(this.storeBase.currentUser)
  }

  handleSubmit(){
    // console.log(this.storeBase.currentUser, 1)
    console.log(this.i18.translate('users.button_add'))

    this.storeBase.select(state => {
      console.log(state.currentUser, 2);
    });
  }

}
