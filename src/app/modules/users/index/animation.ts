import {
  state,
  style,
  animate,
  transition, trigger, useAnimation, animation, keyframes,
} from '@angular/animations';

export enum AnimationState {
  OpenClose = 'openClose',
  Close = 'close',
  Open = 'open'
}

const animationCommon = [
  animate('{{timing}}s 0s',
    keyframes([
      style({
        height: '0px',
        background: 'violet'
      }),
      style({
        height: '100px',
        background: 'violet'
      })
    ])
  ),
  animate('{{timing}}s 0s',
    keyframes([
      style({
        height: '100px',
        background: 'blue'
      }),
      style({
        height: '200px',
        background: 'pink'
      })
    ])
  ),
  animate('{{timing}}s 0s',
    keyframes([
      style({
        height: '100px',
        background: 'violet'
      }),
      style({
        height: '200px',
        background: '#6666c4'
      })
    ])
  )
]

const animations = [
  trigger(AnimationState.OpenClose, [
    state(AnimationState.Open, style({
      height: '{{height}}',
      opacity: 1,
      backgroundColor: 'yellow'
    }), {params: {height: '{{height}}'}}),
    state(AnimationState.Close, style({
      height: '0',
      opacity: 0.8,
      backgroundColor: 'blue'
    })),
    transition(`* => ${AnimationState.Close}`,
      useAnimation(
        animation(
          animate(
            "{{timing}}s",
            keyframes([
              style({
                height: '200px',
                background: 'pink'
              }),
              style({
                height: '100px',
                background: 'violet'
              }),
              style({
                height: '0px',
              }),
            ])
          ),
          {params: {timing: "{{timing}}"}}
        )
      )
    ),
    transition(`* => ${AnimationState.Open}`,[
      useAnimation(
        animation(animationCommon), {params: {timing: '{{timing}}'}})
    ])
  ]),
];

export default animations;

