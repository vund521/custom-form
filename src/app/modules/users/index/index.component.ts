import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {StoreBase} from "core/store/store-base";

import {I18nService, Lang, Language} from "translate-localization";
import {GLOBAL_STATE} from "core/commons/constants";
import animations, {AnimationState} from "./animation";


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations
})
export class IndexComponent {
  readonly languages: Language<Lang>[] = [
    {
      label: 'Tiếng việt',
      lang: 'vi',
      flag: 'assets/svgs/vietnam-flag-icon.svg'
    },
    {
      label: 'English',
      lang: 'en',
      flag: 'assets/svgs/united-kingdom-flag-icon.svg'
    },
  ]

  animationState = AnimationState.Open;

  toggle() {
    if (AnimationState.Open === this.animationState)
      this.animationState = AnimationState.Close;
    else this.animationState = AnimationState.Open
  }

  constructor(@Inject(GLOBAL_STATE)
              private storeBase: StoreBase,
              private i18: I18nService,
              ) {
    storeBase.getInFoUser()
  }
  handleChangeLang(lang: Lang){
    // if (this.i18.lang != lang)
    //   this.i18.use(lang)
  }
}
