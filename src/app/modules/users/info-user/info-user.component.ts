import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {Observer} from "rxjs";
import {GLOBAL_STATE} from "core/commons/constants";
import {StoreBase} from "core/store/store-base";
import {CdkDialogService} from "core/libs/cdk-dialog/cdk-dialog.service";
import {DialogInfoComponent} from "../dialogs/dialog-info/dialog-info.component";
import {UserService} from "services/user.service";
import {IUser} from "core/interfaces";
import {fieldsToFormGroup} from "core/commons/helps/fields-to-form";
import formInit from "./init-form";
import {I18nService} from "translate-localization";
import {BaseComponent} from "../../../core/base/base-component";

@Component({
  selector: 'app-info-user',
  templateUrl: './info-user.component.html',
  styleUrls: ['./info-user.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoUserComponent extends BaseComponent implements OnInit {
  infoUser!: Partial<Observer<IUser>>;
  fields = formInit;
  formData = fieldsToFormGroup(formInit);
  constructor(@Inject(GLOBAL_STATE) public storeBase: StoreBase,
              private i18: I18nService,
              private cdkDialogService : CdkDialogService,
              private readonly us: UserService
              ) {super()}

  ngOnInit(){
    this.us.getInfoUser(1).subscribe((res: Partial<Observer<IUser>>) => {
      this.infoUser = res;
      this.showSuccess('Load dữ liệu thành công');
    });
  }

  user = {
    email: this.i18.asyncTranslate('users.form_user.email'),
    full_name: this.i18.asyncTranslate('users.form_user.name')
  }

  handleOpen(){
    const openDialog = this.cdkDialogService.open(DialogInfoComponent, {
      header: this.i18.translate('users.title'),
      data: {...this.infoUser}
    });

    openDialog.afterClose.subscribe(res => {
      console.log(res)
    })
  }

}

