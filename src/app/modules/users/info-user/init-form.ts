import {Validators} from "@angular/forms";
import {TFormGroup} from "core/types/form";

const {required, minLength, maxLength, email} = Validators;

const formInit = [
  {
    label: 'Tên người dùng',
    name: 'name',
    value: '123',
  },
  {
    label: 'Email người dùng',
    name: 'email',
    value: '456',
    inputType: "email",
    rules: [required, minLength(4), maxLength(8), email],
    messageErrors: [
      {type: "required", message: 'Không được để trống'},
      {type: "minlength", message: 'Không ít hơn 4 ký tự'},
      {type: "maxlength", message: 'Không nhiều hơn 8 ký tự'},
    ]
  },
  {
    label: 'Danh sách công việc',
    name: 'list_job',
    array: [
      {
        label: 'job_FE',
        value: '123',
        name: 'a'
      },
      {
        label: 'job_FE',
        value: '123',
        name: 'a'
      },
      {
        label: 'Level',
        name: 'level',
        group: [
          {
            label: 'Giới thiệu',
            name: 'introduce',
            value: '1234'
          },
          {
            label: 'Level',
            value: '123',
            name: 'level'
          }
        ]
      },
    ]
  },
  {
    label: 'Thông tin CV',
    name: 'info_CV',
    group: [
      {
        label: 'Giới thiệu',
        name: 'introduce',
        value: '1234'
      },
      {
        label: 'Level',
        name: 'level',
        group: [
          {
            label: 'Giới thiệu',
            name: 'introduce',
            value: '1234'
          },
          {
            label: 'Level',
            value: '123',
            name: 'level'
          }
        ]
      },
      {
        label: 'Danh sách công nghệ',
        name: 'list_tech',
        array: [
          {value: 'angular', rules: [required], messageErrors: [{type: "required", message: 'Không để trống'}]},
          {value: 'angular 2', rules: [required], messageErrors: [{type: "required", message: 'Không để trống'}]},
        ]
      }
    ]
  }
] satisfies TFormGroup[];

export default formInit;
