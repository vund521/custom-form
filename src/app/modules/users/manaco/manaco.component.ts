import {Component, ElementRef, Renderer2, ViewChild} from '@angular/core';

@Component({
  selector: 'app-manaco',
  templateUrl: './manaco.component.html',
  styleUrls: ['./manaco.component.css']
})
export class ManacoComponent {
  @ViewChild('view') viewRef!: ElementRef

  tabs = [
    {title: 'Html', language: 'text/html', code: ''},
    {title: 'css', language: 'css', code: ''},
    {title: 'javascript', language: 'javascript', code: ''}
  ];

  editorOptions = {theme: 'vs-dark', language: 'text/html'};

  constructor(private renderer: Renderer2) { }

  changeTab(tab: string){
    if (tab !== this.editorOptions.language)
      this.editorOptions.language = tab;
  }

  run(){
    this.tabs.forEach(tab => {
      switch (tab.language) {
        case 'text/html':
          this.viewRef.nativeElement.innerHTML = tab.code;
          break;
        case 'css':
          const style = this.renderer.createElement('style')
          this.renderer.appendChild(style, this.renderer.createText(tab.code));
          this.renderer.appendChild(document.head, style);
          break;
        default:
          try {
            const result = eval(tab.code);
            console.log("Kết quả:", result, tab.code);
          } catch (error) {
            console.error("Lỗi:", error);
          }
          break;
      }
    })


  }
}
