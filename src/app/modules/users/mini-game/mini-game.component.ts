import {Component, OnInit, signal} from '@angular/core';
import { BehaviorSubject } from 'rxjs';

interface IForm {
  numberOne: number;
  numberTwo: number;
  numberThree: number;
  numberFour: number;
  numberFive: number;
  numberSix: number;
  numberSeven: number;
}

@Component({
  selector: 'app-mini-game',
  templateUrl: './mini-game.component.html',
  styleUrls: ['./mini-game.component.css']
})
export class MiniGameComponent implements OnInit {
  // Khởi tạo BehaviorSubject với một đối tượng hợp lệ
  result$ = new BehaviorSubject<IForm>({
    numberOne: 0,
    numberTwo: 0,
    numberThree: 0,
    numberFour: 0,
    numberFive: 0,
    numberSix: 0,
    numberSeven: 0,
  });
  _result$ = this.result$.asObservable();

  demo = signal<IForm>({
    numberOne: 0,
    numberTwo: 0,
    numberThree: 0,
    numberFour: 0,
    numberFive: 0,
    numberSix: 0,
    numberSeven: 0,
  });

  ngOnInit() {

  }

  handleClick() {
    let count = 0;
    const random = setInterval(() => {
      count++;
      const formGroup = {
        numberOne: this.randomNumber(),
        numberTwo: this.randomNumber(),
        numberThree: this.randomNumber(),
        numberFour: this.randomNumber(),
        numberFive: this.randomNumber(),
        numberSix: this.randomNumber(),
        numberSeven: this.randomNumber()
      };

      this.result$.next(formGroup);
      this.demo.set(formGroup);
      if (count > 50) {
        clearInterval(random);
      }
    }, 100);
  }

  randomNumber() {
    return Math.floor(Math.random() * 10);
  }

  handleSubmit() {}
}
