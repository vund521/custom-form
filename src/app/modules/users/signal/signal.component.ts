import {Component, computed, effect, signal} from '@angular/core';

@Component({
  selector: 'app-signal',
  templateUrl: './signal.component.html',
  styleUrls: ['./signal.component.css']
})
export class SignalComponent {
  count = signal<number>(0);

  x2Count = computed(() => this.count()*2);

  private t = effect(() => this.showLogChange());

  showLogChange(){
    console.log('change_', this.count());
  }

  increment(){
    this.count.update(value => {
      value = this.count() + 1;
      return value
    });
  }
}
