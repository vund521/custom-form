import {User} from "models/user";


const dataFake = [
  {
    "date": 1730345380,
    "note": "note 1",
    "status": false,
    "id": "1"
  },
  {
    "date": 1730345320,
    "note": "note 2",
    "status": false,
    "id": "2"
  },
  {
    "date": 1730345260,
    "note": "note 3",
    "status": false,
    "id": "3"
  },
  {
    "date": 1730345200,
    "note": "note 4",
    "status": false,
    "id": "4"
  },
  {
    "date": 1730345140,
    "note": "note 5",
    "status": false,
    "id": "5"
  },
  {
    "date": 1730345080,
    "note": "note 6",
    "status": false,
    "id": "6"
  },
  {
    "date": 1730345020,
    "note": "note 7",
    "status": false,
    "id": "7"
  },
  {
    "date": 1730344960,
    "note": "note 8",
    "status": false,
    "id": "8"
  },
  {
    "date": 1730344900,
    "note": "note 9",
    "status": false,
    "id": "9"
  },
  {
    "date": 1730344840,
    "note": "note 10",
    "status": false,
    "id": "10"
  },
  {
    "date": 1730344780,
    "note": "note 11",
    "status": false,
    "id": "11"
  },
  {
    "date": 1730344720,
    "note": "note 12",
    "status": false,
    "id": "12"
  },
  {
    "date": 1730344660,
    "note": "note 13",
    "status": false,
    "id": "13"
  },
  {
    "date": 1730344600,
    "note": "note 14",
    "status": false,
    "id": "14"
  },
  {
    "date": 1730344540,
    "note": "note 15",
    "status": false,
    "id": "15"
  },
  {
    "date": 1730344480,
    "note": "note 16",
    "status": false,
    "id": "16"
  },
  {
    "date": 1730344420,
    "note": "note 17",
    "status": false,
    "id": "17"
  },
  {
    "date": 1730344360,
    "note": "note 18",
    "status": false,
    "id": "18"
  },
  {
    "date": 1730344300,
    "note": "note 19",
    "status": false,
    "id": "19"
  },
  {
    "date": 1730344240,
    "note": "note 20",
    "status": false,
    "id": "20"
  },
  {
    "date": 1730344180,
    "note": "note 21",
    "status": false,
    "id": "21"
  },
  {
    "date": 1730344120,
    "note": "note 22",
    "status": false,
    "id": "22"
  },
  {
    "date": 1730344060,
    "note": "note 23",
    "status": false,
    "id": "23"
  },
  {
    "date": 1730344000,
    "note": "note 24",
    "status": false,
    "id": "24"
  },
  {
    "date": 1730343940,
    "note": "note 25",
    "status": false,
    "id": "25"
  },
  {
    "date": 1730343880,
    "note": "note 26",
    "status": false,
    "id": "26"
  },
  {
    "date": 1730343820,
    "note": "note 27",
    "status": false,
    "id": "27"
  },
  {
    "date": 1730343760,
    "note": "note 28",
    "status": false,
    "id": "28"
  },
  {
    "date": 1730343700,
    "note": "note 29",
    "status": false,
    "id": "29"
  },
  {
    "date": 1730343640,
    "note": "note 30",
    "status": false,
    "id": "30"
  },
  {
    "date": 1730343580,
    "note": "note 31",
    "status": false,
    "id": "31"
  },
  {
    "date": 1730343520,
    "note": "note 32",
    "status": false,
    "id": "32"
  },
  {
    "date": 1730343460,
    "note": "note 33",
    "status": false,
    "id": "33"
  },
  {
    "date": 1730343400,
    "note": "note 34",
    "status": false,
    "id": "34"
  },
  {
    "date": 1730343340,
    "note": "note 35",
    "status": false,
    "id": "35"
  },
  {
    "date": 1730343280,
    "note": "note 36",
    "status": false,
    "id": "36"
  },
  {
    "date": 1730343220,
    "note": "note 37",
    "status": false,
    "id": "37"
  },
  {
    "date": 1730343160,
    "note": "note 38",
    "status": false,
    "id": "38"
  },
  {
    "date": 1730343100,
    "note": "note 39",
    "status": false,
    "id": "39"
  },
  {
    "date": 1730343040,
    "note": "note 40",
    "status": false,
    "id": "40"
  },
  {
    "date": 1730342980,
    "note": "note 41",
    "status": false,
    "id": "41"
  },
  {
    "date": 1730342920,
    "note": "note 42",
    "status": false,
    "id": "42"
  },
  {
    "date": 1730342860,
    "note": "note 43",
    "status": false,
    "id": "43"
  },
  {
    "date": 1730342800,
    "note": "note 44",
    "status": false,
    "id": "44"
  },
  {
    "date": 1730342740,
    "note": "note 45",
    "status": false,
    "id": "45"
  },
  {
    "date": 1730342680,
    "note": "note 46",
    "status": false,
    "id": "46"
  },
  {
    "date": 1730342620,
    "note": "note 47",
    "status": false,
    "id": "47"
  },
  {
    "date": 1730342560,
    "note": "note 48",
    "status": false,
    "id": "48"
  },
  {
    "date": 1730342500,
    "note": "note 49",
    "status": false,
    "id": "49"
  },
  {
    "date": 1730342440,
    "note": "note 50",
    "status": false,
    "id": "50"
  },
  {
    "date": 1730342380,
    "note": "note 51",
    "status": false,
    "id": "51"
  },
  {
    "date": 1730342320,
    "note": "note 52",
    "status": false,
    "id": "52"
  },
  {
    "date": 1730342260,
    "note": "note 53",
    "status": false,
    "id": "53"
  },
  {
    "date": 1730342200,
    "note": "note 54",
    "status": false,
    "id": "54"
  },
  {
    "date": 1730342140,
    "note": "note 55",
    "status": false,
    "id": "55"
  },
  {
    "date": 1730342080,
    "note": "note 56",
    "status": false,
    "id": "56"
  },
  {
    "date": 1730342020,
    "note": "note 57",
    "status": false,
    "id": "57"
  },
  {
    "date": 1730341960,
    "note": "note 58",
    "status": false,
    "id": "58"
  },
  {
    "date": 1730341900,
    "note": "note 59",
    "status": false,
    "id": "59"
  }
]

export default  dataFake;
