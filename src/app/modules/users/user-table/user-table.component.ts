import {Component, inject, ViewChild, ViewChildren} from '@angular/core';
import {PaginateComponent} from "core/base/paginate-component";
// import {User} from "models/user";
import dataFake from "./data";
import {ActionTable, ColumnTable} from "core/interfaces/column-table";
import {Store} from "@ngrx/store";
import {selectTask} from "redux/selectors/task.selector";
import {Task} from "models/task";
import * as moment from "moment";
import {Observable} from "rxjs";

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent extends PaginateComponent<Task>{
  private store = inject(Store);
  tasks$!: Observable<Task[]>
  constructor() {
    super();

  }

  ngOnInit(){
    //c1
    // this.tasks$ = this.store.select('task');
    // this.tasks$ = this.store.select(selectTask());
    // this.tasks$.subscribe(res => {
    //   this.dataSource = res;
    // })

    //c2
    this.store.select(selectTask).subscribe(res => {
      this.dataSource = res ;

    })
  }

  actions: ActionTable[] = [
    {
      icon: 'exam-icon',
      action: (row) => {
        console.log(row)
      },
      label: 'Xem chi tiết',
      className: 'btn btn-warning'
    }
  ]

  columns: ColumnTable<Task>[] = [
    {
      title: '#',
      render: (row, index) => this.pageNo(index),
    },
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: 'Note',
      dataIndex: 'note',
      action: (row) => {
        console.log(row)
      }
    },
    {
      title: 'Ngày tháng',
      dataIndex: 'date',
      render: (item) => `${moment(item.date).format('DD/MM/YYYY')}`
    },
    {
      title: 'status',
      render: (row) => (
        `<span class="${row.status ? 'badge bg-primary': 'badge bg-danger'}">
          ${row.status ? 'Active' : 'Non active'}
        </span>`
      )
    }
  ]
}
