import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormUserComponent} from "./form-user/form-user.component";
import {InfoUserComponent} from "./info-user/info-user.component";
import {FormGroupComponent} from "./form-group/form-group.component";
import {IndexComponent} from "./index/index.component";
import {FormArrayComponent} from "./form-array/form-array.component";
import {DecoratorComponent} from "./decorator/decorator.component";
import {DrawFlowComponent} from "./draw-flow/draw-flow.component";
import {ManacoComponent} from "./manaco/manaco.component";
import {SignalComponent} from "./signal/signal.component";
import {MiniGameComponent} from "./mini-game/mini-game.component";
import {UserTableComponent} from "./user-table/user-table.component";

const routes: Routes = [
  {
    path: '',
    component: IndexComponent,
    children: [
      {path: 'info', component: InfoUserComponent},
      {path: 'form-group', component: FormGroupComponent},
      {path: 'form-user', component: FormUserComponent},
      {path: 'form-array', component: FormArrayComponent},
      {path: 'decorator', component: DecoratorComponent},
      {path: 'draw-flow', component: DrawFlowComponent},
      {path: 'manaco', component: ManacoComponent},
      {path: 'signal', component: SignalComponent},
      {path: 'mini-game-lucky', component: MiniGameComponent},
      {path: 'user-table', component: UserTableComponent},
      {path: 'compressor-image',
        loadComponent: () => import('./compressor-image/compressor-image.component')
          .then(c => c.CompressorImageComponent)},
      {path: 'compressor-video',
        loadComponent: () => import('./video-compress/video-compress.component')
          .then(c => c.VideoCompressComponent)}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }


// export const RouterUserModule = RouterModule.forChild(routes);
