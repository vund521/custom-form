import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { CommonModule } from '@angular/common';

import {CoreModule} from "core/core.module";
import {I18nModule} from "translate-localization";
import { MonacoEditorModule } from 'ngx-monaco-editor-v2';
import { UsersRoutingModule } from './users-routing.module';


import {FormTemplateComponent} from "core/components/form-template/form-template.component";

import { FormUserComponent } from './form-user/form-user.component';
import { InfoUserComponent } from './info-user/info-user.component';
import { DialogInfoComponent } from './dialogs/dialog-info/dialog-info.component';
import { FormGroupComponent } from './form-group/form-group.component';
import { IndexComponent } from './index/index.component';
import {FormArrayComponent} from "./form-array/form-array.component";
import {TemplateFormArrayComponent} from "core/components/template-form-array/template-form-array.component";
import { DecoratorComponent } from './decorator/decorator.component';
import {DrawFlowComponent} from "./draw-flow/draw-flow.component";
import { ManacoComponent } from './manaco/manaco.component';
import { SignalComponent } from './signal/signal.component';
import { MiniGameComponent } from './mini-game/mini-game.component';
import { UserTableComponent } from './user-table/user-table.component';
import {DataTableComponent} from "../../core/components/data-table/data-table.component";

@NgModule({
  declarations: [
    FormUserComponent,
    InfoUserComponent,
    DialogInfoComponent,
    FormGroupComponent,
    IndexComponent,
    FormArrayComponent,
    DecoratorComponent,
    DrawFlowComponent,
    ManacoComponent,
    SignalComponent,
    MiniGameComponent,
    UserTableComponent
    // IconDirective
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    I18nModule,
    FormTemplateComponent,
    TemplateFormArrayComponent,
    MonacoEditorModule.forRoot(),
    DataTableComponent,
  ],
})
export class UsersModule { }
