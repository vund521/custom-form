import { ChangeDetectionStrategy, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { fetchFile, toBlobURL } from '@ffmpeg/util';
import FFmpeg from "core/declares/ffmpeg";
import {FileData} from "@ffmpeg/ffmpeg/dist/esm/types";
import {BehaviorSubject, Subject} from "rxjs";

// Sử dụng FFmpeg
const baseURL = "https://unpkg.com/@ffmpeg/core@0.12.6/dist/umd";
@Component({
  selector: 'app-video-compress',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './video-compress.component.html',
  styleUrls: ['./video-compress.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class VideoCompressComponent {
  file!: File;
  loaded = false;

  videoURL$ = new BehaviorSubject('');
  message$ = new Subject();
  constructor() {
  }
  async handleFileUpload(event: Event) {
    const files = (event.target as HTMLInputElement).files;
    if (files && files.length > 0){
      const file = files[0],
        { name, type } = files[0],
        output = 'output.' + type.split('/').pop(),
        input = 'input.' + type.split('/').pop();
      // const chunkSize = 1024 * 1024; // size of each chunk (1MB)
      // let start = 0;
      const ffmpeg = new FFmpeg();
      ffmpeg.on('log', res => {
        this.message$.next(res.message);
        console.log(res.message)
      });

      try {
        this.loaded = true;
        await ffmpeg.load({
          coreURL: await toBlobURL(`${baseURL}/ffmpeg-core.js`, "text/javascript"),
          wasmURL: await toBlobURL(
            `${baseURL}/ffmpeg-core.wasm`,
            "application/wasm"
          ),
          workerURL: await toBlobURL(
            `${baseURL}/ffmpeg-core.worker.js`,
            "text/javascript"
          ),
          // workerURL: await toBlobURL(`${baseURL}/ffmpeg-core.worker.js`, 'text/javascript'),
        });

        await ffmpeg.writeFile(input, await fetchFile(file));
        console.time('exec');
        //lenh doc o day https://ffmpeg.org/ffmpeg.html#Simple-filtergraphs
        await ffmpeg.exec(['-i', input, '-s', '1280x720', '-c:a', 'copy', output]);
        console.timeEnd('exec');
        const data: FileData | any = await ffmpeg.readFile(output);

        if ("buffer" in data) {
          this.videoURL$.next(URL.createObjectURL(new Blob([data.buffer], {type: 'video/mp4'})));
          this.message$.next('Compress thành công !!!!!!!!!!!!!');
        }
      }catch (e) {
        console.log(e)
      }finally
      {
        this.loaded = false;

      }

      // console.log(result)
      // while (start < file.size) {
      //   this.uploadChunk(file.slice(start, start + chunkSize));
      //   start += chunkSize;
      // }
    }
  }

  uploadChunk(chunk: any) {
    const formData = new FormData();
    formData.append('file', chunk);

    // Make a request to the server
    fetch('/upload-endpoint', {
      method: 'POST',
      body: formData,
    });
  }

}
