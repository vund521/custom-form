import {createAction, props} from '@ngrx/store'
import {Task} from "models/task";

const   GET_LIST_TASK = '@Todo/GetAll'

const ADD_TASK = '@Todo/AddTask'

const LOAD_TASK = '@Todo/LoadTask'

const getTask = createAction(GET_LIST_TASK, props<Task>)

const loadTask = createAction(LOAD_TASK, props<{data: Task[]}>())

const addTask = createAction(ADD_TASK, props<Task>())

export {
  getTask,
  addTask,
  loadTask
}
