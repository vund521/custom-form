import {inject, Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {TaskService} from "services/task.service";
import * as TaskAction from "../actions/task.action";
import {catchError, exhaustMap, map, of} from "rxjs";

@Injectable()
export class TaskEffectService {

  private actions$ = inject(Actions);
  private taskService = inject(TaskService)

  loadTask$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TaskAction.getTask),
      exhaustMap(() => this.taskService.getAllData().pipe(
        map(data => TaskAction.loadTask({data})),
        catchError(err => of(err))
      ))
    )
  })
}
