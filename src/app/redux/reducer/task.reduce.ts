import { createReducer, on } from '@ngrx/store';
import * as TaskAction from "redux/actions/task.action";
import {Task} from "models/task";
import {state} from "@angular/animations";

const initialState: Task[] = []

export const taskReducer = createReducer(
  initialState,
  on(TaskAction.getTask, (state) => state),
  on(TaskAction.loadTask
    , (state, payload) => {
    return [...state, ...payload.data];
  }),
  on(TaskAction.addTask, (state, payload) => [...state, payload])
);
