import {TStore} from "../store";
import {createFeatureSelector, createSelector} from "@ngrx/store";
import {Task} from "../../models/task";

// export const selectFeature = (state: TStore) => state.task;
export const selectFeature = createFeatureSelector<Task[]>("task");
export const selectTask =  createSelector(
  selectFeature,
  (state) => state
);
