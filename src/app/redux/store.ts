import {taskReducer} from "./reducer/task.reduce";

const  store = {
  task: taskReducer
}

type TStore = typeof store

export {
  TStore,
  store
}
