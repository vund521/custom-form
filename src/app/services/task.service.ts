import {inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Store} from "@ngrx/store";
import {Task} from "../models/task";

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private url = 'https://5f6d563160cf97001641ab55.mockapi.io/api/v1/todo'

  private http = inject(HttpClient);
  constructor(
  ) { }

  getAllData(){
    return this.http.get<Task[]>(this.url)
  }
}
