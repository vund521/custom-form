import {inject, Injectable} from '@angular/core';
import {ApiService} from "core/services/api.service";

const url = 'users';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _apiService = inject(ApiService);
  constructor() { }

  getInfoUser(id: number){
    return this._apiService.get({
      url: url+'/'+id
    });
  }

  list(){
    return this._apiService.get({url}, true);
  }
}
