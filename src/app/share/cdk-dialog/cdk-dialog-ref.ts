import { ESCAPE, hasModifierKey } from '@angular/cdk/keycodes';
import { OverlayRef } from '@angular/cdk/overlay';
import {BehaviorSubject, Observable, race, Subject} from 'rxjs';
import { filter, mapTo, take } from 'rxjs/operators';
import { CdkDialogContainerComponent } from './cdk-dialog-container.component';
import { AnimationState } from './constants';

const AnimationPhase = {
  START: 'start',
  DONE: 'done'
};

export class CdkDialogRef<TReturnType = any, TContentComponent = any> {
  private readonly beforeClosed$ : any = new Subject();
  private readonly afterClosed$ = new Subject<TReturnType>();
  private result: TReturnType | any;

  componentInstance: CdkDialogContainerComponent<TContentComponent> | any;

  constructor(private readonly overlayRef: OverlayRef) {
    race(
      overlayRef.backdropClick().pipe(mapTo(undefined)),
      overlayRef.keydownEvents().pipe(filter(event => event.keyCode === ESCAPE && !hasModifierKey(event)), mapTo(undefined))
    ).pipe(take(1)).subscribe(this.close.bind(this));

    overlayRef.detachments().subscribe(() => {
      this.afterClosed$.next(this.result);
      this.afterClosed$.complete();
      this.componentInstance = null;
    });
  }

  get beforeClosed(): Observable<unknown> {
    return this.beforeClosed$.asObservable();
  }

  get afterClosed(): Observable<TReturnType> {
    return this.afterClosed$.asObservable();
  }

  close(data?: TReturnType): void {
    this.result = data;
    this.componentInstance.animationStateChanged.pipe(
      filter((event : any) => event.phaseName === AnimationPhase.START),
      take(1)
    ).subscribe(() => {
      this.overlayRef.detachBackdrop(); //để tách backdrop (phần nền) của hộp thoại
      this.beforeClosed$.next();
      this.beforeClosed$.complete();
    });

    //Giải phóng overlay: Sử dụng overlayRef.dispose() để giải phóng overlay khỏi DOM và giải phóng tài nguyên liên quan đến overlay.
    // Điều này có thể được sử dụng khi bạn không cần sử dụng overlay nữa và muốn giải phóng tài nguyên để tránh rò rỉ bộ nhớ hay làm ứng dụng chậm lại.
    this.componentInstance.animationStateChanged.pipe(
      filter((event : any) => event.phaseName === AnimationPhase.DONE && event.toState === AnimationState.Leave),
      take(1)
    ).subscribe(this.overlayRef.dispose.bind(this.overlayRef));

    this.componentInstance.startExitAnimation();
  }
}
