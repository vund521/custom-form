import {OverlayConfig} from "@angular/cdk/overlay";

export class CdkDialogConfig<TData = any> {
  header = ''; // tieu de
  closeable = true; // xac dinh dialog dong hoac khong
  containerAnimationTiming = 0.3; // fade
  contentAnimationTiming = 0.2 // zoom
  animationChildDelay = 0;
  data?: TData;
  overlayConfig?: OverlayConfig;
}
