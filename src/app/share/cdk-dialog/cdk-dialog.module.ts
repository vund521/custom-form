import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkContentDialogDirective } from './cdk-content-dialog.directive';
import { CdkDialogContainerComponent } from './cdk-dialog-container.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    CdkContentDialogDirective,
    CdkDialogContainerComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ]
})
export class CdkDialogModule { }
