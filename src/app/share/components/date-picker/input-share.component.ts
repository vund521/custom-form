import {Component, forwardRef, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  Validator
} from "@angular/forms";

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DatePickerComponent,
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true
    }
  ]
})

export class DatePickerComponent implements ControlValueAccessor, Validator {
  @Input() messageValidates: MessageValidate[] = [];
  @Input() disabled: boolean = false;
  @Input() formatDate: string = 'd/m/Y';
  @Input() enableTime: boolean = false;
  @Input() altInput: boolean = false;
  @Input() convertModelValue: boolean = false;
  @Input() mode: 'single' | 'multiple' | 'range' = 'single';
  public valueInput: ResultInput;
  public control: FormControl = new FormControl();
  onTouched = () => {};
  onChange = (fn: any) => {};

  writeValue(value: ResultInput) {
    // const splitFormat = this.formatDate.split('/');
    this.valueInput = value;
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(onTouched: any) {
    this.onTouched = onTouched;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }
  validate(control: AbstractControl) {
    this.control = control as FormControl;
    return control;
  }

  handleChangeValue(event: Event) {
    const value: string = (event.target as HTMLInputElement).value;
    this.writeValue(value);
    this.onChange(value);
  }

}


type MessageValidate = {
  type: string,
  message: string
}

type ResultInput = string | number| undefined
