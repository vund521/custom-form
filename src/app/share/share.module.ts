import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlatpickrModule } from 'angularx-flatpickr';
import {DatePickerComponent} from "./components/date-picker/input-share.component";
import {CdkDialogModule} from "./cdk-dialog/cdk-dialog.module";
@NgModule({
  imports: [
    CommonModule,
    CdkDialogModule,
    FlatpickrModule.forRoot()
  ],
  declarations: [
    DatePickerComponent,
  ],
    exports: [
        DatePickerComponent,
        CdkDialogModule,
    ]
})
export class ShareModule { }
