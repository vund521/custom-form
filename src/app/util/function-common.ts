const uid = () => Date.now().toString(36) + Math.random().toString(36).substr(2);

interface Obj{}
const isSet = <T = any>(key: string, source: T | any): boolean =>{
  return (key in source);
}

const getValue = <T = any>(key: string, source: T | any, defaultValue: any = null) =>{
  return key in source ? source[key] : defaultValue;
}

export {
  uid,
  isSet,
  getValue
}
